package com.example.samplelist.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.samplelist.data.model.PopularResult


@Dao
interface PopularNewsDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertall(userEntity: List<PopularResult>)



    @Query("SELECT * FROM popularnews")
    fun getAllPopularNews(): List<PopularResult>
}
