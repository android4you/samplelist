package com.example.samplelist.data.model

data class PopularArticleResponse(
        val copyright: String,
        val num_results: Int,
        val results: List<PopularResult>,
        val status: String
)