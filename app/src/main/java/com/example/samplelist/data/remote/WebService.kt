package com.example.samplelist.data.remote

import com.example.samplelist.data.model.PopularArticleResponse
import retrofit2.Response
import retrofit2.http.*

interface WebService {



    @GET("mostpopular/v2/{section}/{period}.json")
    suspend fun getPopularNews(
        @Path("section") section: String,
        @Path("period") period: Int,
        @Query("api-key") apiKey: String
    ):  Response<PopularArticleResponse>
}