package com.example.samplelist.data.repository



import com.example.samplelist.BuildConfig
import com.example.samplelist.data.local.PopularNewsDAO
import com.example.samplelist.data.model.PopularArticleResponse
import com.example.samplelist.data.model.Resource
import com.example.samplelist.data.remote.WebService

import javax.inject.Inject
import javax.inject.Singleton



@Singleton
class ArticleRepository @Inject constructor(
    private val apiService: WebService,
    private val popularDao: PopularNewsDAO
) {

    suspend fun getPopularNews(section: String,
                            period: Int): Resource<PopularArticleResponse> {
        return try {
            val response = apiService.getPopularNews(section, period, BuildConfig.API_KEY)
            if (response.isSuccessful) {
                popularDao.insertall(response.body()?.results!!)
                Resource.Success(response.body())


            } else {
                Resource.Error(Exception(response.message()))

            }
        } catch (ex: Exception) {
            return Resource.Error(ex)
        }
    }
}