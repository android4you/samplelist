package com.example.samplelist.di



import com.example.samplelist.data.remote.WebService
import com.example.samplelist.data.repository.ArticleRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun provideSignupRepository(
        service: WebService
    ): ArticleRepository {
        return ArticleRepository(service)
    }


}