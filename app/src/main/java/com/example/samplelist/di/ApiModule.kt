package com.example.samplelist.di

import android.app.Application
import com.example.samplelist.BuildConfig
import com.example.samplelist.BuildConfig.BASE_URL
import com.example.samplelist.data.local.AppDatabase
import com.example.samplelist.data.remote.WebService
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


    @Module
    @InstallIn(SingletonComponent::class)
    object ApiModule {

        @Singleton
        @Provides
        fun providesDatabase(application: Application) = AppDatabase.invoke(application)

        @Singleton
        @Provides
        fun providesLocationDao(database: AppDatabase) = database.popularNewsDAO()


        @Provides
        @Singleton
        fun provideOkHttpClient(): OkHttpClient {


            val builder = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(25, TimeUnit.SECONDS)
                .readTimeout(25, TimeUnit.SECONDS)


            if (BuildConfig.DEBUG) {
                val logging = HttpLoggingInterceptor()
                logging.level = HttpLoggingInterceptor.Level.BODY
                builder.addInterceptor(logging)

            }


            return builder.build()

        }


        @Provides
        @Singleton
        fun provideRetrofit(client: OkHttpClient, ) : Retrofit{
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()
        }

        @Provides
        @Singleton
        fun provideNewssterService(retrofit: Retrofit): WebService {
            return retrofit.create(WebService::class.java)
        }
    }