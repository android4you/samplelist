package com.example.samplelist.ui.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import com.example.samplelist.data.model.Resource
import com.example.samplelist.databinding.FragmentPopularnewsBinding
import com.example.samplelist.extension.makeGone
import com.example.samplelist.extension.makeVisible
import com.example.samplelist.utils.ConnectivityLiveData

class PopularNewsFragment: Fragment() {


    private lateinit var binding: FragmentPopularnewsBinding
    private val viewModel: PopularNewsViewModel by activityViewModels()

    private lateinit var connectivityLiveData: ConnectivityLiveData

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPopularnewsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        connectivityLiveData= ConnectivityLiveData(requireActivity().application)
        connectivityLiveData.observe(viewLifecycleOwner, Observer {isAvailable->
            when(isAvailable)
            {
                true->{

                }
                false-> {

                }
            }
        })
        fetchPopularNews()
    }


    private fun fetchPopularNews(){
        viewModel.getPopularNews("viewed", 7).observe(viewLifecycleOwner, Observer {
            resource ->
                if (resource is Resource.Loading) {
                    binding.progressBar.makeVisible()
                } else {
                    binding.progressBar.makeGone()
                }
                when (resource) {
                    is Resource.Error -> {
                    //    binding.errorTextView.text = getString(R.string.servicenotavailable)
                        binding.popularList.makeGone()
                        binding.errorTextView.makeVisible()
                    }
                    is Resource.Success -> {
                        if (resource.data?.status=="OK") {
                            binding.popularList.makeVisible()
                            binding.errorTextView.makeGone()

                        } else {
                          //  binding.errorTextView.text = resource.data.message
                            binding.popularList.makeGone()
                            binding.errorTextView.makeVisible()
                        }
                    }
                    else -> {
                    }
                }
            })

    }

}