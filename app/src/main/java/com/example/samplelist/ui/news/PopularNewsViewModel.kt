package com.example.samplelist.ui.news

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.samplelist.data.model.Resource
import com.example.samplelist.data.repository.ArticleRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject



@HiltViewModel
class PopularNewsViewModel @Inject constructor(
    private val repository: ArticleRepository
) : ViewModel() {



    fun getPopularNews(section: String, period: Int) =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(Resource.Loading())
            emit(repository.getPopularNews(section, period))

        }

}